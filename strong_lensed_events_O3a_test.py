#!/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/python
__authors__='sumit.k, haris.k'
"""
This is the driver script which takes posterior samples as input and 
calculate Bayes factor
Created by Sumit Kumar on 2018-09-09
Last modified on 2020-04-21
"""
import numpy as np, os, sys, glob, json, h5py, argparse
from optparse import OptionParser
sys.path.insert(0,'../../src')
import lensing_utils as lensing
from pesummary.gw.file.read import read

def read_pe_samples(fname,event_name):
	data = read(fname)
	if event_name=='GW190412' or event_name=='GW190814':
		posterior_samples = data.samples_dict['C01:IMRPhenomPv3HM']
	elif event_name=='GW190521':
		posterior_samples = data.samples_dict['C01:NRSur7dq4']
	else:
		posterior_samples = data.samples_dict['C01:IMRPhenomPv2']
	return posterior_samples
        

parser = argparse.ArgumentParser(description='This is stand alone code fo computing strong lensing bayesfactors for a pair of BBH events from the  posterior files')
parser.add_argument('-posterior_file1','--posterior_file1', help='file containing the posterior samples from the event 1', required=True)
parser.add_argument('-posterior_file2','--posterior_file2', help='file containing the posterior samples from the event 2', required=True)
parser.add_argument('-out_file', '--out_file', help="Output ASCII file name", default='out.txt')
parser.add_argument('-event1_tag','--event1_tag',help="Tag for event 1 e.g. GW150914", default='Event1')
parser.add_argument('-event2_tag','--event2_tag',help="Tag for event 2 e.g. GW151226", default='Event2')

args = parser.parse_args()
posterior_file1 = args.posterior_file1
posterior_file2 = args.posterior_file2
out_file = args.out_file
event1_tag = args.event1_tag
event2_tag = args.event2_tag
lensed_dt_prior_file = '../../data/prior/dominik_powerlaw1_delta_t_secs.dat'
lensed_dt_prior_file_anupreeta = '../../data/prior/tdel_prob_anupreeta_d66a6b6b.txt'

if posterior_file1.endswith('.dat') and posterior_file2.endswith('.dat'):
	#List of parameters used for computing Bayesfactor
	params = ['m1', 'm2', 'ra', 'dec', 'a1', 'a2', 'costilt1', 'costilt2', 'costheta_jn']
	# Prior ranges for parameter mass_1,mass_2,ra,dec,a_1,a_2,cos_tilt_1,cos_tilt_2,cos_theta_jn
	prior_min  = [2.,2.,0,-1.,0,0,-1.,-1.,-1.]
	prior_max  = [200.,200.,2*np.pi,1,1,1,1,1,1]
	obs_time =  15811200 # Total O3a observation time 

	print("reading posterior samples")
	print('%s:'%event1_tag)
	posterior_samples1 = np.genfromtxt(posterior_file1,names=True)
	print('%s:'%event2_tag)
	posterior_samples2 = np.genfromtxt(posterior_file2,names=True)
	print("...done")
	tc1,tc2 = np.median(posterior_samples1['time']), np.median(posterior_samples2['time'])
else:
	#List of parameters used for computing Bayesfactor
	params = ['mass_1','mass_2','ra','dec','a_1','a_2','cos_tilt_1','cos_tilt_2','cos_theta_jn']
	# Prior ranges for parameter mass_1,mass_2,ra,dec,a_1,a_2,cos_tilt_1,cos_tilt_2,cos_theta_jn
	prior_min  = [2.,2.,0,-1.,0,0,-1.,-1.,-1.]
	prior_max  = [200.,200.,2*np.pi,1,1,1,1,1,1]
	obs_time =  15811200 # Total O3a observation time 
	
	print("reading posterior samples")
	print('%s:'%event1_tag)
	posterior_samples1 = read_pe_samples(posterior_file1)
	print('%s:'%event2_tag)
	posterior_samples2 = read_pe_samples(posterior_file2)
	print("...done")
	tc1,tc2 = np.median(posterior_samples1['geocent_time']), np.median(posterior_samples2['geocent_time'])

print("reading time delay prior")
dt_prior_samples = np.genfromtxt(lensed_dt_prior_file,dtype=None,names=True)
time_data = np.genfromtxt(lensed_dt_prior_file_anupreeta,dtype=None,names=True)
dt_anupreeta = time_data['t_sec']
p_dt = time_data['Poft_Rzmin']
print("...done")


print("...calculating bayes factors:")
ff=lensing.bayes_factor(posterior_samples1,posterior_samples2,dt_prior_samples,dt_anupreeta,p_dt,tc1,tc2)

print("calculating bayes factor R_lu")
Rlu1,Rlu2 = ff.calc_rlu(obs_time)
print("...done")
print("Bayes factor R_lu:  %f\t%f"%(Rlu1,Rlu2))


print("calculating bayes factor B_lu for m1, m2, ra, dec, a1, a2, costilt1, costilt2 and costheta_jn:")
Blu = ff.calc_blu(params, prior_min, prior_max)
print("Bayes factor B_lu:  %f"%Blu)
print("...done")

print("saving results in out_file")
with open(out_file, "w") as file:
    file.write('%s\t%s\t%.2e\t%.2e\t%.2e'%(event1_tag,event2_tag,Blu,Rlu1,Rlu2))
print("...done")


